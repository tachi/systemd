#!/bin/sh

set -e

_systemctl() {
    if [ -z "$DPKG_ROOT" ] && [ -d /run/systemd/system ]; then
        systemctl "$@"
    fi
}

_update_catalog() {
    journalctl ${DPKG_ROOT:+--root="$DPKG_ROOT"} --update-catalog || true
}

_update_binfmt() {
    _systemctl restart systemd-binfmt.service || true
}

_restart_managers() {
    _systemctl daemon-reexec || true
    # Re-exec user instances so that running user managers are updated too.
    # SIGRTMIN+25 -> reexec. Note that this is asynchronous, but we can't use
    # D-Bus as dbus-user-session is not guaranteed to be available.
    _systemctl kill --kill-whom='main' --signal='SIGRTMIN+25' 'user@*.service' || true
    # do not restart logind
    # https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=919509
    _systemctl try-restart systemd-networkd.service || true
    _systemctl try-restart systemd-journald.service || true
}

# Update Message Catalogs database and binfmt registrations in response to dpkg triggers
if [ "$1" = "triggered" ]; then
    shift
    for trigger in $@; do
        case $trigger in
            /usr/lib/systemd/catalog)
                _update_catalog
                ;;
            /usr/lib/binfmt.d)
                _update_binfmt
                ;;
            libc-upgrade)
                _restart_managers
                ;;
        esac
    done
    exit 0
fi

# Enable getty, remote-fs.target and systemd-pstore by default on new installs
if [ -z "$2" ]; then
    systemctl ${DPKG_ROOT:+--root="$DPKG_ROOT"} enable getty@tty1.service || true
    systemctl ${DPKG_ROOT:+--root="$DPKG_ROOT"} enable remote-fs.target || true
    systemctl ${DPKG_ROOT:+--root="$DPKG_ROOT"} enable systemd-pstore.service || true
fi

# Create /etc/machine-id
systemd-machine-id-setup ${DPKG_ROOT:+--root="$DPKG_ROOT"}

# Enable persistent journal, in auto-mode, by default on new installs
if [ -z "$2" ]; then
    mkdir -p "$DPKG_ROOT/var/log/journal"
fi

# Initial update of the Message Catalogs database
_update_catalog

# Move the old locale file into /etc. Symlinks will be created by tmpfiles.d later
if [ -f "$DPKG_ROOT/etc/default/locale" ] && [ ! -L "$DPKG_ROOT/etc/default/locale" ] && [ ! -f "$DPKG_ROOT/etc/locale.conf" ]; then
    mv "$DPKG_ROOT/etc/default/locale" "$DPKG_ROOT/etc/locale.conf"
fi

# On upgrade for now keep the current tmpfiles.d (as it used to be after patches),
# drop in Forky
if [ -n "$2" ] && dpkg --compare-versions "$2" lt 256~rc3-3 && [ ! -f "$DPKG_ROOT/etc/tmpfiles.d/tmp.conf" ]; then
    mkdir -p "$DPKG_ROOT/etc/tmpfiles.d/"
    echo 'D /tmp 1777 root root -' > "$DPKG_ROOT/etc/tmpfiles.d/tmp.conf"
fi

# We used to ship tmp.mount in /usr/share as an example, and some users link it in /etc/,
# remove the link as a workaround until Trixie ships. This method would also have created
# a link in local-fs.target.wants, so remove that too.
if [ -n "$2" ] && [ -L "$DPKG_ROOT/etc/systemd/system/tmp.mount" ] && [ "$(readlink "$DPKG_ROOT/etc/systemd/system/tmp.mount")" = "/usr/share/systemd/tmp.mount" ]; then
    rm -f "$DPKG_ROOT/etc/systemd/system/tmp.mount"
    rm -f "$DPKG_ROOT/etc/systemd/system/local-fs.target.wants/tmp.mount"
fi

# The user may have used 'systemctl enable /usr/share/systemd/tmp.mount', which
# would have created a symlink in the local-fs.target.wants directory (as the
# pre-Trixie tmp.mount included an [install] section for that purpose). If it
# exists, remove it.
if [ -n "$2" ] && [ -L "$DPKG_ROOT/etc/systemd/system/local-fs.target.wants/tmp.mount" ] && [ "$(readlink "$DPKG_ROOT/etc/systemd/system/local-fs.target.wants/tmp.mount")" = "/usr/share/systemd/tmp.mount" ]; then
    rm -f "$DPKG_ROOT/etc/systemd/system/local-fs.target.wants/tmp.mount"
fi

# Unless it's already running, ensure /tmp/ does not get overwritten by
# the tmpfs from tmp.mount in case a unit is later activated that implicitly
# depends on it (for example with PrivateTmp=yes) by runtime masking it
if ! _systemctl is-active --quiet tmp.mount; then
    _systemctl mask --runtime tmp.mount
fi

#DEBHELPER#

if [ -n "$2" ]; then
    _restart_managers
fi
